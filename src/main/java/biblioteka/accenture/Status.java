package biblioteka.accenture;

public enum Status {
        RENT("Rent"), IN_STOCK("In Stock");

        private String readableName;

        private Status(String readableName){
            this.readableName = readableName;
        }

        public String getReadableName() {
            return readableName;
        }
    }

