package biblioteka.accenture;

import java.time.LocalDate;

public class Person {
        private String name;
        private String subname;
        private LocalDate birthDate;


        public Person(String name, String subname, LocalDate birthDate) {
            this.name = name;
            this.subname = subname;
            this.birthDate = birthDate;
        }





        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSubname() {
            return subname;
        }

        public void setSubname(String subname) {
            this.subname = subname;
        }

        public LocalDate getBirthDate() {
            return birthDate;
        }

        public void setBirthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
        }
    }

