package biblioteka.accenture;

public class Tools {
    public static Book createBook(String name, String subname, String title,
                                     String isbn){
        Autor autor = new Autor(name, subname);
        Book book = new Book(title, isbn, autor);
        return book;
    }
}
