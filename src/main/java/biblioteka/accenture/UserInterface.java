package biblioteka.accenture;

import java.time.LocalDate;

public class UserInterface {
    public static void main(String[] args) {
        Libary b1 = new Libary();

        addBooksToLibary(b1);
        Book javaPodstawy = Tools.createBook("Ray", "Horstmann",
                "Java Podstawy. Wydanie X", "1234567890");

        System.out.println(b1.showBookList());



        System.out.println("=========================");


        b1.createReader(javaPodstawy, new Person("Jan",
                "Gnom",
                LocalDate.of(1979,05,25)));
        System.out.println(b1.showWhoRent(javaPodstawy));
        b1.createReader(javaPodstawy, new Person("Anna",
                "Pacha",
                LocalDate.of(1979,05,25)));
        System.out.println(b1.showWhoRent(javaPodstawy));
        System.out.println("=========================");



        System.out.println(b1.listOfBooksRentByUser
                (new Person("Jan", "Kowalski", null)));


    }

    private static void addBooksToLibary(Libary b1) {

        b1.addBook(Tools.createBook("Cay", "Horstmann",
                "Java Techniki Zaawansowane", "2345678901"));
        b1.addBook(Tools.createBook("Adam", "Mickiewicz",
                "Pan Tadeusz", "4567890123"));
        b1.addBook(Tools.createBook("Thomas", "Pynchon",
                "Tecza Grawitacji", "83838383838"));
        b1.addBook(Tools.createBook("James", "Joyce",
                "Ulisses", "822378901"));
        b1.addBook(Tools.createBook("Konrad", "Kissin",
                "Cmy", "2346708901"));
        b1.addBook(Tools.createBook("Stanislaw", "Lem",
                "Fiasko", "2377778901"));
        b1.addBook(Tools.createBook("Samuel", "Beckett",
                "Molloy", "7775678901"));
        b1.addBook(Tools.createBook("Marcel", "Proust",
                "W poszukiwaniu Straconego czasu", "25555678901"));
        b1.addBook(Tools.createBook("Tove", "Jansson",
                "Zima w Dolinie Muminkow", "2345678901"));
        b1.addBook(Tools.createBook("Hugo", "Draxler",
                "Somnolentus Wydalacz", "23446678901"));
        b1.addBook(Tools.createBook("Ardrey", "Struggacki",
                "Slimak na zboczu", "23678878951"));

    }


}

