package biblioteka.accenture;

public class Autor {
    private String name;
    private String subname;


    public Autor(String name, String subname) {
        this.name = name;
        this.subname = subname;

    }

    public String getName() {
        return name;
    }

    public void setName(String imie) {
        this.name = name;
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname;
    }
}
