package biblioteka.accenture;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Libary {
    private List<Book> bookList;

    private Map<Book, List<Person>> readersList;

    public Libary() {
        this.bookList = new ArrayList<>();
        this.readersList = new HashMap<>();
    }

    public void addBook(Book book) {
        if (book != null) {
            bookList.add(book);
        }
    }

    public void createReader(Book rentBook,
                             Person reader) {
        if (readersList.containsKey(rentBook)) {
            readersList.get(rentBook).add(reader);
        } else {
            readersList.put(rentBook,
                    new ArrayList<Person>());
            readersList.get(rentBook).add(reader);
        }
    }

    public List<String> showWhoRent(Book book) {
        List<String> results = new ArrayList<>();
        if (readersList.containsKey(book)) {
            List<Person> userFromList = readersList.get(book);
            for (Person person : userFromList) {
                results.add(person.getName() + " " + person.getSubname());
            }
            return results;
        } else {
            return results;
        }
    }

    public List<Book> listOfBooksRentByUser(Person person) {
        List<Book> results = new ArrayList<>();
        for (Book book : readersList.keySet()) {
            List<Person> readers = readersList.get(book);
            for (int i = 0; i < readers.size(); i++) {
                if (readers.get(i).equals(person)) {
                    results.add(book);
                    break;
                }
            }
        }
        return results;
    }


    public List<String> showBookList() {
        List<String> result = new ArrayList<>();
        for (Book book : bookList) {
            String form = String.format("%s, %s. ",
                    book.getAutor().toString(), book.getTitle());

            result.add(form);
        }
        return result;
    }


}
